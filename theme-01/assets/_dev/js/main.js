// Redefine $ as jQuery
var $ = jQuery;

console.log('Javscript Loaded!');

(function($) {
	$(document).ready(function() {
		/*
		* Page load actions
		*/

		/*
		* Event listeners
		*/

		$('.accordion_link').on('click', function(e){
			e.preventDefault();
			accordion(e);
		})

	});
})(jQuery);

/* Function file includes */

// Accordion
//@prepros-append parts/accordion.js
