
<footer>
  <div class="section primary-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <img src="https://placehold.it/200x60"/>
        </div>
        <div class="col-sm-4">
          <h5>Contact us</h5>
          <hr>
          <div class="address">
            <div class="icon pin-white"></div>
            <p><strong>Your company name</strong></p>
            <p>Your company name, </br> 2nd line, </br> City/Town, </br> Postcode</p>
          </div>
          <div class="phone">
            <div class="icon phone-white"></div>
            <p><strong>00000 0000000</strong></p>
          </div>
          <div class="email">
            <div class="icon email-white"></div>
            <p><strong>info@yourcompany.co.uk</strong></p>
          </div>
        </div>
        <div class="col-sm-2 sub-nav">
          <h5>Links</h5>
          <hr>
          <ul>
            <li><a href="">Home</a></li>
            <li><a href="">About us</a></li>
            <li><a href="">Services</a></li>
            <li><a href="">Products</a></li>
            <li><a href="">Case Studies</a></li>
            <li><a href="">Our Team</a></li>
            <li><a href="">Contact Us</a></li>
        </div>
        <div class="col-sm-2 social-icons">
          <h5>Social</h5>
          <hr>
          <ul>
            <li><a href=""><span class="icon linkedin"></span>Linkedin</a></li>
            <li><a href=""><span class="icon twitter"></span>Twitter</a></li>
            <li><a href=""><span class="icon facebook"></span>Facebook</a></li>
            <li><a href=""><span class="icon instagram"></span>Instagram</a></li>
        </div>
      </div>
    </div>
  </div>

  <div class="section secondary-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <p>© Copyright 2018 - your company name</p>
        </div>
        <div class="col-sm-4">
          <p><a href="https://citcom.co.uk/" target="_blank">Designed by <strong>Citizen Communications</strong></a></p>
        </div>
      </div>
    </div>
  </div>
</footer>

  <?php wp_footer(); ?>

  </body>
</html>
