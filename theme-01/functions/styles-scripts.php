<?php
add_action('wp_enqueue_scripts', 'enqueue_theme_scripts');
add_action('wp_enqueue_scripts', 'enqueue_theme_styles');
add_action('wp_enqueue_scripts', 'enqueue_child_styles');
add_action('wp_enqueue_scripts', 'enqueue_child_scripts');

function enqueue_theme_scripts() {
	$parent_dir = get_template_directory_uri();

	if (is_admin()) {
		wp_enqueue_script();
	}
}

function enqueue_theme_styles() {
	$parent_dir = get_template_directory_uri();

	if (is_admin()) {
		wp_enqueue_style('parent-admin-css', "$parent_dir/assets/css/admin.css");
	}

	wp_enqueue_style('parent-css', "$parent_dir/style.css");
}

function enqueue_child_styles() {
    wp_enqueue_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/assets/css/styles.min.css', array('bootstrap-css'), true);
}

function enqueue_child_scripts() {
	wp_enqueue_script('bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), true);
  wp_enqueue_script( 'child-script-main', get_stylesheet_directory_uri() . '/assets/js/main.min.js', array( 'jquery' ), true);
}
