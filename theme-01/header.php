<?php
$site_url = site_url();

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />

	<title><?php echo bloginfo('name'); ?></title>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header>

		<nav class="navbar navbar-default">
		  <div class="container">

		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#"><img src="https://placehold.it/200x60"/></a>
		    </div>

		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav navbar-right">
						<li><a href="/">Home</a></li>
						<li><a href="/about-us">About us</a></li>
						<li><a href="/services">Services</a></li>
						<li><a href="/products">Products</a></li>
						<li><a href="/case-studies">Case Studies</a></li>
						<li><a href="/our-team">Our Team</a></li>
						<li><a href="/contact-us">Contact Us</a></li>
		      </ul>
		    </div>
		  </div>
		</nav>

	</header>
