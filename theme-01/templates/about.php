<?php
/*
Template Name: About
*/
get_header(); ?>

<?php get_template_part('templates/parts/about/about', 'banner'); ?>
<?php get_template_part('templates/parts/about/about', 'intro'); ?>
<?php get_template_part('templates/parts/about/about', 'history'); ?>
<?php get_template_part('templates/parts/about/about', 'future'); ?>

<?php get_footer(); ?>
