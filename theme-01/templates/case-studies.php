<?php
/*
Template Name: Case Studies
*/
get_header(); ?>
<div class="case-studies">
  <?php get_template_part('templates/parts/case-studies/case-studies', 'banner'); ?>
  <?php get_template_part('templates/parts/case-studies/case-studies', 'content'); ?>
</div>

<?php get_footer(); ?>
