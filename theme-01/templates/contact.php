<?php
/*
Template Name: Contact
*/
get_header(); ?>

<?php get_template_part('templates/parts/contact/contact', 'banner'); ?>
<?php get_template_part('templates/parts/contact/contact', 'content'); ?>

<?php get_footer(); ?>
