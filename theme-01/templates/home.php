<?php
/*
Template Name: Home
*/
get_header(); ?>

<?php get_template_part('templates/parts/home/home', 'banner'); ?>
<?php get_template_part('templates/parts/home/home', 'cta'); ?>
<?php get_template_part('templates/parts/home/home', 'intro'); ?>
<?php get_template_part('templates/parts/home/home', 'services'); ?>
<?php get_template_part('templates/parts/home/home', 'testimonials'); ?>
<?php get_template_part('templates/parts/home/home', 'meet-the-team'); ?>
<?php get_template_part('templates/parts/home/home', 'accreditation'); ?>

<?php get_footer(); ?>
