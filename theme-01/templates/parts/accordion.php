<div class="panel-group accordion" id="accordion" role="tablist" aria-mutiselectable="true">

  <div class="panel panel-default accordion_panel">
    <div class="panel-heading accordion_heading-tab" role="tab" id="heading1">
      <h4 class="panel-title accordion_title">
        <a class="accordion_link" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-controls="collapse1">
          Title #1
        </a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse accordion_panel-collapse" role="tabpanel" aria-labelledby="heading1">
      <div class="panel-body accordion_panel-body">
         Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
      </div>
    </div>
  </div>

  <div class="panel panel-default accordion_panel">
    <div class="panel-heading accordion_heading-tab" role="tab" id="heading2">
      <h4 class="panel-title accordion_title">
        <a class="accordion_link" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-controls="collapse2">
          Title #2
        </a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse accordion_panel-collapse" role="tabpanel" aria-labelledby="heading2">
      <div class="panel-body accordion_panel-body">
         Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
         <ul class="list-group accordion_list-group">
           <li class="list-group-item accordion_list-group-item">One</li>
           <li class="list-group-item accordion_list-group-item">Two</li>
           <li class="list-group-item accordion_list-group-item">Three</li>
           <li class="list-group-item accordion_list-group-item">Four</li>
      </div>
    </div>
  </div>

</div>
