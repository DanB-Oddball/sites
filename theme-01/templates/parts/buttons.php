<h3>Default buttons</h3>

<a href="#" role="button" class="btn btn--dark">Button</a>
<a href="#" role="button" class="btn btn--light">Button</a>
<a href="#" role="button" class="btn btn--dark-outline">Button</a>
<a href="#" role="button" class="btn btn--light-outline">Button</a>

<a href="#" role="button" class="btn btn--dark btn--large">Button</a>
<a href="#" role="button" class="btn btn--light btn--large">Button</a>
<a href="#" role="button" class="btn btn--dark-outline btn--large">Button</a>
<a href="#" role="button" class="btn btn--light-outline btn--large">Button</a>

<h3>Rounded corner buttons</h3>

<a href="#" role="button" class="btn btn--rounded btn--dark">Button</a>
<a href="#" role="button" class="btn btn--rounded btn--light">Button</a>
<a href="#" role="button" class="btn btn--rounded btn--dark-outline">Button</a>
<a href="#" role="button" class="btn btn--rounded btn--light-outline">Button</a>

<a href="#" role="button" class="btn btn--rounded btn--dark btn--large">Button</a>
<a href="#" role="button" class="btn btn--rounded btn--light btn--large">Button</a>
<a href="#" role="button" class="btn btn--rounded btn--dark-outline btn--large">Button</a>
<a href="#" role="button" class="btn btn--rounded btn--light-outline btn--large">Button</a>

<h3>Pill buttons</h3>

<a href="#" role="button" class="btn btn--pill btn--dark">Button</a>
<a href="#" role="button" class="btn btn--pill btn--light">Button</a>
<a href="#" role="button" class="btn btn--pill btn--dark-outline">Button</a>
<a href="#" role="button" class="btn btn--pill btn--light-outline">Button</a>

<a href="#" role="button" class="btn btn--pill btn--dark btn--large">Button</a>
<a href="#" role="button" class="btn btn--pill btn--light btn--large">Button</a>
<a href="#" role="button" class="btn btn--pill btn--dark-outline btn--large">Button</a>
<a href="#" role="button" class="btn btn--pill btn--light-outline btn--large">Button</a>

<h3>Text buttons (links)</h3>

<a href="#" role="button" class="btn-text">Text Button</a>
<a href="#" role="button" class="btn-text btn-text--upper">Text Button</a>
<a href="#" role="button" class="btn-text btn-text--icon">Text Button</a>
<a href="#" role="button" class="btn-text btn-text--upper btn-text--icon">Text Button</a>
<a href="#" role="button" class="btn-text btn--block">Text Button</a>
<a href="#" role="button" class="btn-text btn-text--upper btn--block">Text Button</a>
<a href="#" role="button" class="btn-text btn-text--icon btn--block">Text Button</a>
<a href="#" role="button" class="btn-text btn-text--upper btn-text--icon btn--block">Text Button</a>
