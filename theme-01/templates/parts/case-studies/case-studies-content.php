<div class="section section--center-text">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Case Studies</h2>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
      </div>
    </div>
    <div class="row case-studies__list">
      <div class="col-sm-4">
        <article class="case-studies__single">
          <img class="case-studies__image img-responsive" src="https://placehold.it/360x145"/>
          <div class="case-studies__inner">
            <h3 class="case-studies__title">Boilers</h3>
            <p class="case-studies__description">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
            <a href="#" role="button" class="case-studies__link btn-text btn-text--upper btn-text--icon btn--block">Learn More</a>
          </div>
        </article>
      </div>
      <div class="col-sm-4">
        <article class="case-studies__single">
          <img class="case-studies__image img-responsive" src="https://placehold.it/360x145"/>
          <div class="case-studies__inner">
            <h3 class="case-studies__title">Boilers</h3>
            <p class="case-studies__description">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
            <a href="#" role="button" class="case-studies__link btn-text btn-text--upper btn-text--icon btn--block">Learn More</a>
          </div>
        </article>
      </div>
      <div class="col-sm-4">
        <article class="case-studies__single">
          <img class="case-studies__image img-responsive" src="https://placehold.it/360x145"/>
          <div class="case-studies__inner">
            <h3 class="case-studies__title">Boilers</h3>
            <p class="case-studies__description">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
            <a href="#" role="button" class="case-studies__link btn-text btn-text--upper btn-text--icon btn--block">Learn More</a>
          </div>
        </article>
      </div>
    </div>

  </div>
</div>
