<div class="section section--center-text">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Request a call back</h2>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
      </div>
    </div>
  </div>
</div>
<div class="section section--center-text section--secondary">
  <div class="section__inner container">
    <div class="contact-details">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="contact-details__title">Contact Details</h3>
          <hr>
        </div>
        <div class="col-sm-6">
            <div class="contact-details__address">
              <div class="icon pin"></div>
              <p>Your company address,<br> 2nd line,<br> City/town,<br> Postcode</p>
            </div>
            <div class="contact-details__phone">
              <div class="icon phone"></div>
              <p>01234 567891</p>
            </div>
            <div class="contact-details__email">
              <div class="icon email"></div>
              <p><a href="mailto:info@yourcompany.co.uk?Subject=Your%20Company%20Subject" target="_top">info@yourcompany.co.uk</a></p>
            </div>
            <div class="contact-details__time">
              <div class="icon time"></div>
              <p><strong>Opening times:</strong></p>
              <p>Mon-Thurs 8:00am - 5:30pm</p>
              <p>Friday 8:00am - 3:00pm</p>
              <p>Sat-Sun Closed</p>
            </div>
        </div>
        <div class="col-sm-6">
          <form id="contact_form" class="contact-form">
            <div class="form-group">
              <label for="name" class="sr-only">Name</label>
              <input id="name" type="text" name="name" placeholder="Name">
            </div>
            <div class="form-group">
              <label for="number" class="sr-only">Phone Number</label>
              <input id="number" type="text" name="phone_number" placeholder="Phone Number">
            </div>
            <div class="form-group">
              <label for="email" class="sr-only">Email</label>
              <input id="email" type="text" name="email" placeholder="Email Address">
            </div>
            <div class="form-group">
              <label for="message" class="sr-only">Message</label>
              <textarea id="message" name="message" placeholder="Insert your message here"></textarea>
            </div>
            <div class="form-group">
              <label for="callback" class="sr-only">Prefered time for call back</label>
              <select class="form-control" id="callback">
                <option>Anytime</option>
                <option>AM</option>
                <option>PM</option>
                <option>Other (Please include what time you're avalible in the message)</option>
              </select>
            </div>

            <button type="submit" href="#" role="button" class="btn btn--rounded btn--primary btn--center">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
