<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <div class="select-style">
      <select>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputFile">File input</label>

    <!-- <a href="#" role="button" class="btn btn--dark">Upload a file</a> -->
    <div class="upload-btn-wrapper">
      <button class="btn btn--dark">Upload a file</button>
      <input type="file" id="exampleInputFile">
    </div>
    <p class="text--info p--small-print">Example block-level help text here.</p>
  </div>
  <div class="checkbox">
    <label>Check box?
      <input type="checkbox">
      <span class="checkmark"></span>
    </label>
  </div>
  <div class="radio">
    <label>
      Option one is this and that&mdash;be sure to include why it's great
      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
      <span class="checkmark"></span>
    </label>
  </div>
  <div class="radio">
    <label>
      Option two can be something else and selecting it will deselect option one
      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
      <span class="checkmark"></span>
    </label>
  </div>
</form>
