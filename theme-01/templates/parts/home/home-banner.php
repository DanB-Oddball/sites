<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner/banner-gas.jpg)">
      <div class="carousel-caption">
        <h1>Your company name</h1>
        <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        <a href="#" role="button" class="btn btn--rounded btn--dark btn--center">Button</a>
      </div>
    </div>
    <div class="item" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner/banner-gas.jpg)">
      <div class="carousel-caption">
        <h2>2nd title</h2>
        <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        <a href="#" role="button" class="btn btn--rounded btn--dark btn--center">2nd Button</a>
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div style="display: none;" class="section section--banner section--center-text">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h1>Your company name</h1>
        <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        <a href="#" role="button" class="btn btn--rounded btn--dark">Button</a>
      </div>
    </div>

  </div>
</div>
