<div class="section section--primary section--center-text">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Get a free quote</h2>
        <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        <a href="#" role="button" class="btn btn--rounded btn--light-outline btn--center">Get Quote</a>
      </div>
    </div>

  </div>
</div>
