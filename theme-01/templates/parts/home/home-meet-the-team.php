<div class="section section--center-text">
  <div class="section__inner container">
    <div class="section__team">
      <div class="row">
        <div class="section__team-tab">
          <div class="section__team-single col-sm-4">
            <img class="section__team-member-img img-responsive" src="https://placehold.it/280x240"/>
            <h3 class="section__team-member-name">Joe Bloggs</h3>
            <h4 class="section__team-member-job">Senior Service Engineer</h4>
          </div>
          <div class="section__team-single col-sm-4">
            <img class="section__team-member-img img-responsive" src="https://placehold.it/280x240"/>
            <h3 class="section__team-member-name">Joe Bloggs</h3>
            <h4 class="section__team-member-job">Senior Service Engineer</h4>
          </div>
          <div class="section__team-single col-sm-4">
            <img class="section__team-member-img img-responsive" src="https://placehold.it/280x240"/>
            <h3 class="section__team-member-name">Joe Bloggs</h3>
            <h4 class="section__team-member-job">Senior Service Engineer</h4>
          </div>
          <div class="col-xs-12">
            <a href="#" role="button" class="btn btn--rounded btn--dark btn--center">See More</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
