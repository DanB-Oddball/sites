<div class="section section--center-text services">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Services</h2>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
      </div>
    </div>

    <div class="row service-section">
      <div class="col-sm-4 service-section__item">
        <img src="https://placehold.it/120"/>
        <h3>Installation</h3>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
      </div>
      <div class="col-sm-4 service-section__item">
        <img src="https://placehold.it/120"/>
        <h3>Maintainance</h3>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
      </div>
      <div class="col-sm-4 service-section__item">
        <img src="https://placehold.it/120"/>
        <h3>Repairs</h3>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
      </div>
    </div>

  </div>
</div>
