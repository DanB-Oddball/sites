<div class="section section--center-text section--secondary">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Testimonials</h2>
        <div class="separator vertical"></div>
        <p>We are really proud of our work, but don’t just take our word for it…</p>
      </div>
      <div class="col-xs-12 testimonials">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <p class="testimonial__quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
              <p class="testimonial__name"><strong>A. N. Other</strong></p>
              <p class="testimonial__type">Happy Customer</p>
            </div>

            <div class="item">
              <p class="testimonial__quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
              <p class="testimonial__name"><strong>A. N. Other</strong></p>
              <p class="testimonial__type">Happy Customer</p>
            </div>

            <div class="item">
              <p class="testimonial__quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
              <p class="testimonial__name"><strong>A. N. Other</strong></p>
              <p class="testimonial__type">Happy Customer</p>
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>

  </div>
</div>
