<div class="section section--center-text">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Our Services</h2>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
      </div>
    </div>

  </div>
</div>

<div class="section section--center-text section--secondary">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Installation</h2>
        <div class="separator vertical"></div>
      </div>
      <div class="col-sm-6 margin-top-bottom">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
        <a href="#" role="button" class="btn btn--rounded btn--primary btn--center">Contact Us</a>
      </div>
      <div class="col-sm-6">
        <img class="img-responsive" src="https://placehold.it/450"/>
      </div>
    </div>

  </div>
</div>

<div class="section section--center-text">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Maintainance</h2>
        <div class="separator vertical"></div>
      </div>
      <div class="col-sm-6">
        <img class="img-responsive" src="https://placehold.it/450"/>
      </div>
      <div class="col-sm-6 margin-top-bottom">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
        <a href="#" role="button" class="btn btn--rounded btn--primary btn--center">Contact Us</a>
      </div>
    </div>

  </div>
</div>

<div class="section section--center-text section--secondary">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Repairs</h2>
        <div class="separator vertical"></div>
      </div>
      <div class="col-sm-6 margin-top-bottom">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
        <a href="#" role="button" class="btn btn--rounded btn--primary btn--center">Contact Us</a>
      </div>
      <div class="col-sm-6">
        <img class="img-responsive" src="https://placehold.it/450"/>
      </div>
    </div>

  </div>
</div>

<div class="section section--center-text ">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>FAQs</h2>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt...</p>
      </div>
      <div class="col-sm-12">
        <div class="panel-group accordion" id="accordion" role="tablist" aria-mutiselectable="true">

          <div class="panel panel-default accordion_panel">
            <a class="accordion_link" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-controls="collapse1">
              <div class="panel-heading accordion_heading-tab" role="tab" id="heading1">
                <h4 class="panel-title accordion_title">
                    Title #1
                </h4>
              </div>
            </a>
            <div id="collapse1" class="panel-collapse collapse accordion_panel-collapse" role="tabpanel" aria-labelledby="heading1">
              <div class="panel-body accordion_panel-body">
                 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
              </div>
            </div>
          </div>

          <div class="panel panel-default accordion_panel">
            <a class="accordion_link" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-controls="collapse2">
              <div class="panel-heading accordion_heading-tab" role="tab" id="heading2">
                <h4 class="panel-title accordion_title">
                    Title #2
                </h4>
              </div>
            </a>
            <div id="collapse2" class="panel-collapse collapse accordion_panel-collapse" role="tabpanel" aria-labelledby="heading2">
              <div class="panel-body accordion_panel-body">
                 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                 <ul class="list-group accordion_list-group">
                   <li class="list-group-item accordion_list-group-item">One</li>
                   <li class="list-group-item accordion_list-group-item">Two</li>
                   <li class="list-group-item accordion_list-group-item">Three</li>
                   <li class="list-group-item accordion_list-group-item">Four</li>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>

  </div>
</div>
