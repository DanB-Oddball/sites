<div class="section section--center-text">
  <div class="section__inner container">

    <div class="row">
      <div class="col-xs-12">
        <h2>Meet the Team</h2>
        <div class="separator vertical"></div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
      </div>
    </div>

    <div class="section__team">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs nav-justified section__team-nav" role="tablist">
        <li role="presentation" class="active"><a href="#engineers" aria-controls="engineers" role="tab" data-toggle="tab">Engineers</a></li>
        <li role="presentation"><a href="#installers" aria-controls="installers" role="tab" data-toggle="tab">Installers</a></li>
        <li role="presentation"><a href="#office" aria-controls="office" role="tab" data-toggle="tab">Office</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="section__team-tab tab-pane active" id="engineers">
          <div class="row">
            <div class="section__team-single col-sm-4">
              <img class="section__team-member-img img-responsive" src="https://placehold.it/280x240"/>
              <h3 class="section__team-member-name">Joe Bloggs</h3>
              <h4 class="section__team-member-job">Senior Service Engineer</h4>
            </div>
            <div class="section__team-single col-sm-4">
              <img class="section__team-member-img img-responsive" src="https://placehold.it/280x240"/>
              <h3 class="section__team-member-name">Joe Bloggs</h3>
              <h4 class="section__team-member-job">Senior Service Engineer</h4>
            </div>
            <div class="section__team-single col-sm-4">
              <img class="section__team-member-img img-responsive" src="https://placehold.it/280x240"/>
              <h3 class="section__team-member-name">Joe Bloggs</h3>
              <h4 class="section__team-member-job">Senior Service Engineer</h4>
            </div>
            <div class="section__team-single col-sm-4">
              <img class="section__team-member-img img-responsive" src="https://placehold.it/280x240"/>
              <h3 class="section__team-member-name">Joe Bloggs</h3>
              <h4 class="section__team-member-job">Senior Service Engineer</h4>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="section__team-tab tab-pane" id="installers">2</div>
        <div role="tabpanel" class="section__team-tab tab-pane" id="office">3</div>
      </div>
    </div>


<tab
  </div>
</div>
