<h3> Typography elements</h3>

<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>

<h1 class="h--upper">Heading 1</h1>
<h2 class="h--upper">Heading 2</h2>
<h3 class="h--upper">Heading 3</h3>
<h4 class="h--upper">Heading 4</h4>
<h5 class="h--upper">Heading 5</h5>
<h6 class="h--upper">Heading 6</h6>

<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi .</p>

<p class="p--small-print">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi .</p>

<div class="blockquote blockquote--marks">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</div>
<div class="blockquote blockquote--indent">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</div>

<div class="textbox textbox--filled">
  <h5>Textbox heading</h5>
  <h6>Texbox subheading</h6>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
  <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
</div>

<div class="textbox textbox--outline textbox--rounded">
  <h5>Textbox heading</h5>
  <h6>Texbox subheading</h6>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
  <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
</div>

<h4>Text states</h4>

<p class="text--success">Success Text : Grumpy wizards make toxic brew for the evil Queen and Jack.</p>
<p class="text--info">Info Text : Grumpy wizards make toxic brew for the evil Queen and Jack.</p>
<p class="text--warning">Warning Text : Grumpy wizards make toxic brew for the evil Queen and Jack.</p>
<p class="text--danger">Danger Text : Grumpy wizards make toxic brew for the evil Queen and Jack.</p>
<p class="text--danger text--small p--small-print">Danger Text Small: Grumpy wizards make toxic brew for the evil Queen and Jack.</p>

<h4>Text lists</h4>

<ul class="list">
  <li>Home</li>
  <li>About Us</li>
  <li>Services</li>
  <li>Blog</li>
  <li>Meet the Team</li>
  <li>Contact Us</li>
</ul>

<ul class="list list--nopoints">
  <li>Home</li>
  <li>About Us</li>
  <li>Services</li>
  <li>Blog</li>
  <li>Meet the Team</li>
  <li>Contact Us</li>
</ul>

<ul class="list list--nopoints list--upper">
  <li>Home</li>
  <li>About Us</li>
  <li>Services</li>
  <li>Blog</li>
  <li>Meet the Team</li>
  <li>Contact Us</li>
</ul>

<ul class="list">
  <li>Home</li>
  <li>About Us</li>
    <ul>
      <li>The Company</li>
      <li>Our history</li>
      <li>The Future</li>
      <li>Contact Details</li>
    </ul>
</ul>

<ol class="list">
  <li>Home</li>
  <li>About Us</li>
  <li>Services</li>
  <li>Blog</li>
  <li>Meet the Team</li>
  <li>Contact Us</li>
</ol>
