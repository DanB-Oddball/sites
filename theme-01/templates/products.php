<?php
/*
Template Name: Products
*/
get_header(); ?>
<div class="products">
  <?php get_template_part('templates/parts/products/products', 'banner'); ?>
  <?php get_template_part('templates/parts/products/products', 'content'); ?>
</div>

<?php get_footer(); ?>
