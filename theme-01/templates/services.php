<?php
/*
Template Name: Services
*/
get_header(); ?>

<?php get_template_part('templates/parts/services/services', 'banner'); ?>
<?php get_template_part('templates/parts/services/services', 'content'); ?>

<?php get_footer(); ?>
