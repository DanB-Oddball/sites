<?php
/*
Template Name: Styleguide
*/
get_header(); ?>

<h1>Styleguide</h1>

<?php include_once('parts/accordion.php'); ?>
<?php include_once('parts/form.php'); ?>
<?php include_once('parts/typography.php'); ?>
<?php include_once('parts/buttons.php'); ?>

<?php get_footer(); ?>
