<?php
/*
Template Name: Our Team
*/
get_header(); ?>

<?php get_template_part('templates/parts/team/team', 'banner'); ?>
<?php get_template_part('templates/parts/team/team', 'content'); ?>

<?php get_footer(); ?>
