// Redefine $ as jQuery
var $ = jQuery;

(function($) {
	$(document).ready(function() {
		/*
		* Page load actions
		*/
		// cardHeight(true);

		/*
		* Event listeners
		*/
		// $(window).on('resize', function(e) {
		// 	cardHeight();
		// })


		$(document).scroll(function () {
			backToTop();
		})

	});
})(jQuery);

/* Function file includes */

// back to top
//@prepros-append parts/module-back-to-top.js

// accordion
//@prepros-append parts/module-accordion.js

// screen size change functions
//@prepros-append parts/module-screen-size-change.js
