	<div class="back-to-top-button">
		<a href="#bodyTop" title="Go back to the top of this page" role="button">Top</a>
	</div>

	<footer>
		<!-- details -->
		<div class="section section--brand-3">
			<div class="container details">
				<div class="row">
					<div class="col-sm-6 col-md-3">
						<img class="footer-logo" src="https://placehold.it/211x63?text=logo" alt=""/>
						<div class="copyright">
							<p>&#169; Copyright <?php echo Date("Y"); ?> - your company name </p>
							<p>Design <a href="https://citcom.co.uk" title="Design by Citizen Communication" target="_blank">Citizen Communication</a></p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<h3 class="h--line">Address</h3>
						<p>
							Address line one,<br/>
							Address line two,<br/>
							City/town,<br/>
							County,<br/>
							Postcode
						</p>
					</div>
					<div class="col-sm-6 col-md-3">
						<h3 class="h--line">Phone</h3>
						<p>
							00000 000000 (free quote)<br/>
							00000 000000 (repairs)
						</p>
					</div>
					<div class="col-sm-6 col-md-3">
						<h3 class="h--line">Email</h3>
						<p>
							info@yourcompany.co.uk<br/>
							repairs@yourcompany.co.uk
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- footer nav -->

		<div class="section section--brand-2 section--padding-xsmall">
			<div class="container">
				<div class="row">

					<div class="col-sm-12">
						<nav class="footer-nav">
							<ul>
								<?php get_template_part('templates/parts/footer-nav'); ?>
							</ul>
						</nav>
					</div>

				</div>
			</div>
		</div>

	</footer>

	<?php wp_footer(); ?>

	</body>
</html>
