<?php
if (is_admin()) {
	add_action('admin_enqueue_scripts', 'enqueue_child_admin_scripts');
	add_action('admin_enqueue_styles', 'enqueue_child_admin_styles');
}

add_action('wp_enqueue_scripts', 'enqueue_parent_scripts');
add_action('wp_enqueue_scripts', 'enqueue_parent_styles');
add_action('wp_enqueue_scripts', 'enqueue_child_scripts');
add_action('wp_enqueue_scripts', 'enqueue_child_styles');

// Modify script loading method
add_action('script_loader_tag', 'modify_script_loading', 10, 2);

function enqueue_child_admin_scripts() {
	wp_enqueue_script('js-child-admin', get_stylesheet_directory_uri() . '/assets/js/admin.min.js', array('jquery'), false, true);
}

function enqueue_child_styles() {
		wp_enqueue_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
		wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/assets/css/styles.min.css', array('bootstrap-css'), true);
}

function enqueue_child_scripts() {
	global $post;
	$parent_dir = get_stylesheet_directory_uri();

	wp_enqueue_script('bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), false, true);
	wp_enqueue_script('js-child-main', "$parent_dir/assets/js/main.min.js", array('jquery'), false, true);

	// Add vars to front end
	wp_localize_script('js-child-main', 'theme', array('siteurl' => trailingslashit(site_url()), 'ajaxurl' => admin_url('admin-ajax.php')));

	// if (isset($post->post_name)) {
	// 	// Load G Map
	// 	if ($post->post_name === 'contact') {
	// 		wp_enqueue_script('js-map', "$parent_dir/assets/js/map.min.js");
	// 		wp_enqueue_script('js-lib-gmap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDCbN1hYrNP68gtsVG-Ui-Z5S-u8V5PF8w&callback=initMap', ['js-map'], false, true);
	// 	}
	// }
}

function enqueue_parent_scripts() {
	$parent_dir = get_template_directory_uri();

	if (is_admin()) {
		wp_enqueue_script('js-parent-admin', "$parent_dir/assets/js/admin.min.js", array('jquery'));
	}
}

function enqueue_parent_styles() {
	$parent_dir = get_template_directory_uri();

	if (is_admin()) {
		wp_enqueue_style('parent-admin-css', "$parent_dir/assets/css/admin.min.css");
	}

	wp_enqueue_style('parent-css', "$parent_dir/style.css");
}

/**
* Change scripts loading method.
*/
function modify_script_loading($html, $handle) {
	$scripts = [
		'js-lib-gmap' => [
			'async' => true,
			'defer' => true
		]
	];

	if (array_key_exists($handle, $scripts)) {
		if ($scripts[$handle]['async']) {
			$html = str_replace(' src', ' async="async" src', $html);
		}

		if ($scripts[$handle]['defer']) {
			$html = str_replace(' src', ' defer="defer" src', $html);
		}
	}

	return $html;
}
