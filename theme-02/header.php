<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<!-- <meta name="format-detection" content="telephone=no"> -->

	<title>Worcester Bosch Theme 02</title>

	<?php wp_head(); ?>
</head>
<body id="bodyTop" <?php body_class(); ?>>
	<header>
		<nav class="navbar navbar-default">
			<div class="container nav-container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar-container">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</span>
						<span class="mobile-icon-bar-container">
							<span class="mobile-icon-bar">x</span>
						</span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url(); ?>">
						<img alt="Logo" src="https://placehold.it/134x40"/>
					</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<?php get_template_part('templates/parts/header-nav'); ?>
					</ul>
				</div>
			</div>
		</nav>
	</header>
