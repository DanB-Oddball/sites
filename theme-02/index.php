<?php get_header(); ?>

<main class="page-content">

	<?php if (have_posts()): ?>
		<?php while (have_posts()): ?>
			<h1><?php the_title(); ?></h1>
			<?php
			the_post();
			the_title();
			the_content();
		?>
		<?php endwhile; ?>
	<?php else: ?>
		<h1><?php echo bloginfo('title'); ?></h1>
	<?php endif; ?>

</main>

<?php get_footer(); ?>
