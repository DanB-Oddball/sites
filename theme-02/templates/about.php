<?php
/*
Template Name: About
*/
get_header();

// Banner
get_template_part('templates/parts/about/banner');

// Content
get_template_part('templates/parts/about/content');

?>

<div class="section section--grey">
	<div class="container">
		<div class="section--inner">
			<div class="row">
				<?php get_template_part('templates/parts/accreditation'); ?>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
