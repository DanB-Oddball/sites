<?php
/*
Template Name: Case Studies
*/
get_header();

// Banner
get_template_part('templates/parts/case-studies/banner');

// content
get_template_part('templates/parts/case-studies/content');

get_footer();
