<?php
/*
Template Name: Contact
*/
get_header();

// Banner
get_template_part('templates/parts/contact/banner');

// content
get_template_part('templates/parts/contact/content');

get_footer();
