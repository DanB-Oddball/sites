<?php
/*
Template Name: Home
*/
get_header();

// Banner
get_template_part('templates/parts/home/banner');

// styleguide
// get_template_part('templates/styleguide');


// intro
get_template_part('templates/parts/home/intro');

// intro
?>
<!-- HTML is here because the about page has a accreditation section but grey background on the section -->
<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">
				<?php get_template_part('templates/parts/accreditation'); ?>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
