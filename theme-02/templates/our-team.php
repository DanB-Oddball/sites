<?php
/*
Template Name: Our Team
*/
get_header();

// Banner
get_template_part('templates/parts/our-team/banner');

// content
get_template_part('templates/parts/our-team/content');

get_footer();
