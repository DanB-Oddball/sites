<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">The Company</h2>
					<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>


				<div class="col-sm-4">
					<img alt="" class="img-responsive section__image" src="https://placehold.it/292"/>
				</div>

				<div class="col-sm-8">
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<blockquote>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
					</blockquote>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Our History</h2>
				</div>

				<div class="col-sm-8">
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<blockquote>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
					</blockquote>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-sm-4">
					<img alt="" class="img-responsive section__image" src="https://placehold.it/326x467"/>
				</div>

			</div>
		</div>
	</div>
</div>
