<?php
$site_url = site_url();
$templateDIR = 'templates/';
?>

<li>
	<a title="Go to the home page" href="<?php echo $site_url; ?>"
	class="<?php echo (is_page_template($templateDIR.'home.php'))? 'active' : ''; ?>">
		Home
	</a>
</li>
<li>
	<a title="Go to the about us page" href="<?php echo $site_url; ?>/about-us"
	class="<?php echo (is_page_template($templateDIR.'about.php'))? 'active' : ''; ?>">
		About Us
	</a>
</li>
<li>
	<a title="Go to the services page" href="<?php echo $site_url; ?>/services"
	class="<?php echo (is_page_template($templateDIR.'services.php'))? 'active' : ''; ?>">
		Services
	</a>
</li>
<li>
	<a title="Go to the products page" href="<?php echo $site_url; ?>/products"
	class="<?php echo (is_page_template($templateDIR.'products.php'))? 'active' : ''; ?>">
		Products
	</a>
</li>
<li>
	<a title="Go to the case studies page" href="<?php echo $site_url; ?>/case-studies"
	class="<?php echo (is_page_template($templateDIR.'case-studies.php'))? 'active' : ''; ?>">
		Case Studies
	</a>
</li>
<li>
	<a title="Go to the our team page" href="<?php echo $site_url; ?>/our-team"
	class="<?php echo (is_page_template($templateDIR.'our-team.php'))? 'active' : ''; ?>">
		Our Team
	</a>
</li>
<li>
	<a title="Go to the contact page" href="<?php echo $site_url; ?>/contact"
	class="<?php echo (is_page_template($templateDIR.'contact.php'))? 'active' : ''; ?>">
		Contact
	</a>
</li>
