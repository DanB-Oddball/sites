<div id="carousel-home" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#carousel-home" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-home" data-slide-to="1"></li>
		<li data-target="#carousel-home" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner_grad.svg);">
		<div class="item active" >
			<div class="container">
				<div class="carousel-content row">
					<div class="col-md-7">
						<h1>content 1</h1>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
					</div>
					<div class="col-md-5">
						<img class="img-responsive" src="https://placehold.it/432x303" alt=""/>
					</div>
				</div>
			</div>
		</div>
		<div class="item" >
			<div class="container">
				<div class="carousel-content row">
					<div class="col-md-7">
						<h2>content 2</h2>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
					</div>
					<div class="col-md-5">
						<img class="img-responsive" src="https://placehold.it/432x303" alt=""/>
					</div>
				</div>
			</div>
		</div>
		<div class="item" >
			<div class="container">
				<div class="carousel-content row">
					<div class="col-md-7">
						<h2>content 3</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>
					<div class="col-md-5">
						<img class="img-responsive" src="https://placehold.it/432x303" alt=""/>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
