<!-- home -->
<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">About Us</h2>
					<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-sm-8">
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<blockquote>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
					</blockquote>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<a href="" title="" role="button" class="btn btn--primary">Read More</a>
				</div>

				<div class="col-sm-4">
					<img alt="" class="img-responsive section__image" src="https://placehold.it/292"/>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section section--grey">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Our Services</h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>


				<div class="col-sm-12">
					<div class="tab-section">
						<ul class="tab-section__nav tab-nav__vertical nav nav-pills nav-stacked col-sm-3">
							<li class="active"><a href="#tab_a" data-toggle="pill">Installation</a></li>
							<li><a href="#tab_b" data-toggle="pill">Maintainance</a></li>
							<li><a href="#tab_c" data-toggle="pill">Servicing &amp; Repairs</a></li>
						</ul>
						<div class="tab__content tab__content--vertical tab-content col-sm-9">
							<div class="tab-pane active" id="tab_a">
								<h3 class="h--line">Installation</h3>
								<p>Installation Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
							</div>
							<div class="tab-pane" id="tab_b">
								<h3 class="h--line">Maintainance</h3>
								<p>Maintainance Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
							</div>
							<div class="tab-pane" id="tab_c">
								<h3 class="h--line">Servicing and Repairs</h3>
								<p>Servicing and Repairs Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
							</div>
						</div><!-- tab content -->
					</div>
				</div>

				<div class="col-sm-12">
					<a href="" title="" role="button" class="btn btn--primary">Read More</a>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Meet the Team</h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-sm-12">
					<div class="card-team">
						<div class="tab-section">
							<ul class="tab-section__nav tab-nav__team nav nav-pills">
								<li class="active">
									<a href="#tab-team_a" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_b" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_c" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_d" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
							</ul>
							<div class="tab__content tab-nav__team tab-content">
								<div class="tab-pane active" id="tab-team_a">
									<div class="card-team__inner">
										<h3>Bob Hoskins</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_b">
									<div class="card-team__inner">
										<h3>Name Two</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_c">
									<div class="card-team__inner">
										<h3>Name Three</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_d">
									<div class="card-team__inner">
										<h3>Name Four</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
							</div><!-- tab content -->
							<a href="" title="" role="button" class="btn btn--primary">See More</a>
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section section--image section--content-light newsletter" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner_01.jpg);">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Newsletter</h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<form id="formNewsletter">
						<div class="form-group">
							<label><span class="sr-only">Email Address</span>
								<input type="email" name="email" placeholder="Email address"/>
							</label>
						</div>
						<button class="btn btn--secondary btn--block">Sign-UP</button>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Case Studies</h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi .</p>
				</div>

				<!-- 2 col -->

				<div class="col-sm-6">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>

				<!-- 3 col -->

				<!-- <div class="col-sm-6 col-md-4">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-4">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div> -->

				<!-- 4 col -->

				<!-- <div class="col-sm-6 col-md-3">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div> -->

				<!-- 5 col -->

				<!-- <div class="col-xs-12 col-sm-4 col-md-5ths">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-5ths">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-5ths">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-push-2 col-md-push-0 col-sm-4 col-md-5ths">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-push-2 col-md-push-0 col-sm-4 col-md-5ths">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Case study</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div> -->

			</div>
		</div>
	</div>
</div>

<div class="section section--grey">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Testimonials</h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

					<div id="testimonialsCarousel" class="testimonials-carousel carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#testimonialsCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#testimonialsCarousel" data-slide-to="1"></li>
							<li data-target="#testimonialsCarousel" data-slide-to="2"></li>
						</ol>

						<div class="carousel-inner">
							<div class="item active">
								<blockquote>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</blockquote>
								<p>Happy Customer, Worcester</p>
							</div>

							<div class="item">
								<blockquote>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</blockquote>
								<p>Happy Customer, Worcester</p>
							</div>

							<div class="item">
								<blockquote>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</blockquote>
								<p>Happy Customer, Worcester</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>
