<div class="banner-section"
style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner_grad.svg);">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 banner__col">
				<div class="banner__content">
					<h1 class="page-title"><?php echo get_the_title(); ?></h1>
					<p class="page-subtitle">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
				</div>
			</div>
		</div>
	</div>
</div>
