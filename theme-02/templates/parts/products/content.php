<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Products</h2>
					<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-sm-8">
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<blockquote>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
					</blockquote>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-sm-4">
					<img alt="" class="img-responsive section__image" src="https://placehold.it/292"/>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section section--no-padding-top">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-6">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Boilers</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Boiler Controls</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Cylinders</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="card">
						<img alt="" class="img-responsive card__image" src="https://placehold.it/720x345"/>
						<div class="card__inner">
							<h3 class="h--line card__title">Pumps</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
