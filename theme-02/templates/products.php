<?php
/*
Template Name: Products
*/
get_header();

// Banner
get_template_part('templates/parts/products/banner');

// content
get_template_part('templates/parts/products/content');

get_footer();
