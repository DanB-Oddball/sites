<?php
/*
Template Name: Services
*/
get_header();

// Banner
get_template_part('templates/parts/services/banner');

// content
get_template_part('templates/parts/services/content');

get_footer();
