// Redefine $ as jQuery
var $ = jQuery;

(function($) {
	$(document).ready(function() {
		/*
		* Page load actions
		*/
		// cardHeight(true);

		/*
		* Event listeners
		*/
		// $(window).on('resize', function(e) {
		// 	cardHeight();
		// })

		if($("header").hasClass("home-menu")) {
			$(".collapse").on('show.bs.collapse', function() {
				navbarHome(true);
				navbar(true);
			});
			$(".collapse").on('hide.bs.collapse', function() {
				navbarHome(false);
				navbar(false);
			});
		} else {
			$(".collapse").on('show.bs.collapse', function () {
				navbar(true);
			});
			$(".collapse").on('hide.bs.collapse', function () {
				navbar(false);
			});
		}


		$(document).scroll(function () {
			backToTop();
		})

	});
})(jQuery);

/* Function file includes */

// back to top
//@prepros-append parts/module-back-to-top.js

// accordion
//@prepros-append parts/module-accordion.js

// screen size change functions
//@prepros-append parts/module-screen-size-change.js

// navbar functions
//@prepros-append parts/module-menu.js

// flaoting contact tab
//@prepros-append parts/module-floating-contact.js
