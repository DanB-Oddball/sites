function backToTop(){
	var btn = $('.back-to-top-button');
	var screenHeight = window.innerHeight;
	var scroll = $(document).scrollTop();

	if(scroll >= (screenHeight / 2)) {
		btn.fadeIn();
	} else {
		btn.fadeOut()
	}
}
