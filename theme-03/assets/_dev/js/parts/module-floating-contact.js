function floatContact(){
	var contactTab = $('.block-contact');
	var screenHeight = window.innerHeight;
	var scroll = $(document).scrollTop();

	if(scroll >= (screenHeight / 2)) {
		contactTab.fadeIn();
	} else {
		contactTab.fadeOut()
	}
}
