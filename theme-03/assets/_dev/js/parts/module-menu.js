function navbarHome (state) {
	if(state) {
		// show
		$('header.home-menu').addClass('full');
		$('header.home-menu .navbar-brand').removeClass('navbar-brand--small');

	} else {
		// hide
		$(".collapse").on('hidden.bs.collapse', function() {
			$('header.home-menu').removeClass('full');
			$('header.home-menu .navbar-brand').addClass('navbar-brand--small');
		});
	}
}

function navbar (state) {
	if(state) {
		// show
		$(".icon-bar-container").hide();
		$(".mobile-icon-bar-container").show();
	} else {
		// hide
		$(".collapse").on('hidden.bs.collapse', function() {
			$(".icon-bar-container").show();
			$(".mobile-icon-bar-container").hide();
		});
	}
}
