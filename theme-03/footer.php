	<div class="back-to-top-button">
		<a href="#bodyTop" title="Go back to the top of this page" role="button">Top</a>
	</div>

	<footer>
		<!-- details -->
		<div class="section">
			<div class="container details">
				<div class="row">
					<div class="col-sm-6 col-md-2">
						<img class="footer-logo img-responsive" src="https://placehold.it/211x63?text=logo" alt=""/>
						<p>
							Address line 1,<br/>
							Address line 2,<br/>
							City,<br/>
							County,<br/>
							Post Code
						</p>
					</div>
					<div class="col-sm-6 col-md-3">
						<p>01234 567890</p>
						<p>01234 567890</p>
						<p>support@companyname.co.uk</p>
					</div>
					<div class="col-sm-6 col-md-3 col-md-push-1">
						<h4>Links</h4>
						<hr>
						<nav class="footer-nav">
							<ul>
								<?php get_template_part('templates/parts/footer-nav'); ?>
							</ul>
						</nav>
					</div>
					<div class="col-sm-6 col-md-3 col-md-push-1">
						<h4>Social</h4>
						<hr>
						<div class="social-container">
							<div class="social-container-link">
								<a href="#" title="">
									<img src="https://placehold.it/40" alt=""/>
									<span>LinkedIn</span>
								</a>
								<a href="#" title="">
									<img src="https://placehold.it/40" alt=""/>
									<span>Twitter</span>
								</a>
								<a href="#" title="">
									<img src="https://placehold.it/40" alt=""/>
									<span>Facebook</span>
								</a>
								<a href="#" title="">
									<img src="https://placehold.it/40" alt=""/>
									<span>Instagram</span>
								</a>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="copyright">
						<p>&#169; Copyright <?php echo Date("Y"); ?> - your company name </p>
						<p>Design <a href="https://citcom.co.uk" title="Design by Citizen Communication" target="_blank">Citizen Communication</a></p>
					</div>
				</div>
			</div>
		</div>

	</footer>

	<?php wp_footer(); ?>

	</body>
</html>
