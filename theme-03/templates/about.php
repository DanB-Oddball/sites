<?php
/*
Template Name: About
*/
get_header();

// Banner
get_template_part('templates/parts/about/banner');

// Content
get_template_part('templates/parts/about/content');

?>

<?php get_footer(); ?>
