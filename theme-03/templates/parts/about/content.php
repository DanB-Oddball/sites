<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-4">
					<img alt="" class="img-responsive section__image" src="https://placehold.it/400x260"/>
				</div>

				<div class="col-sm-8 content--padding-md-left">
					<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section section--grad" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/grad.svg);">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-5">
					<h2 class="h--line">Areas we cover</h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<ul class="list">
						<li><span class="list__icon list__icon--tick"></span>Birmingham</li>
						<li><span class="list__icon list__icon--tick"></span>Wolverhampton</li>
						<li><span class="list__icon list__icon--tick"></span>Walsall</li>
						<li><span class="list__icon list__icon--tick"></span>Stafford</li>
						<li><span class="list__icon list__icon--tick"></span>Telford</li>
						<li><span class="list__icon list__icon--tick"></span>Dudley</li>
					</ul>
				</div>

				<div class="col-sm-7">
					<img alt="" class="img-responsive section__image map-section" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/map.png"/>
				</div>

			</div>
		</div>
	</div>
</div>
