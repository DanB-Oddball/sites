<div class="section section--grey">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Get in Touch</h2>
					<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<?php echo do_shortcode('[contact-form-7 id="25" title="Contact form"]'); ?>

			</div>
		</div>
	</div>
</div>

<div class="section contact-details">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Contact Details</h2>
					<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-sm-4">
					<img alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/location.svg" class="img-responsive icon-contact icon-location"/>
					<address>
						Your company address,<br />
						2nd line,<br />
						City/Town,<br />
						Postcode,<br />
						Country
					</address>
				</div>

				<div class="col-sm-4">
					<img alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/phone.svg" class="img-responsive icon-contact icon-number"/>
					<p>
						00000 000000 (quote)<br />
						00000 000000 (emergency)
					</p>
				</div>

				<div class="col-sm-4">
					<img alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/email.svg" class="img-responsive icon-contact icon-email"/>
					<p><a href="mailto:info@yourcompany.co.uk" title="">info@yourcompany.co.uk</a><br />
					<a href="mailto:info@yourcompany.co.uk" title="">quotes@yourcompany.co.uk</a><br />
					<a href="mailto:info@yourcompany.co.uk" title="">repairs@yourcompany.co.uk</a>
				</p>
				</div>

			</div>
		</div>
	</div>
</div>


<div class="contact-map">
	<img alt="" src="https://placehold.it/1920x1000" class="img-responsive"/>
</div>
