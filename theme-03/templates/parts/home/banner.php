<div class="intro">

	<div class="intro__left"></div>


	<div class="intro__right">

		<header class="home-menu">
			<nav class="navbar navbar-default">
				<div class="container nav-container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar-container">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</span>
							<span class="mobile-icon-bar-container">
								<svg version="1.1" overflow="visible" preserveAspectRatio="none" viewBox="0 0 45 45" width="43" height="43" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g transform="translate(1, 1)"><title>Shape1EBF4A22-EFED-4B9E-898D-D9BD14667547</title><desc>Created with Sketch.</desc><defs/><g id="Design" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="MENU" transform="translate(-1263.000000, -47.000000)" fill="#A47542" fill-rule="nonzero"> <g id="times" transform="translate(1263.000000, 47.000000)"> <path d="M29.6504545,21.5 L41.8749148,9.27553977 C43.3750284,7.77542614 43.3750284,5.34323864 41.8749148,3.84190341 L39.1580966,1.12508523 C37.657983,-0.375028409 35.2257955,-0.375028409 33.7244602,1.12508523 L21.5,13.3495455 L9.27553977,1.12508523 C7.77542614,-0.375028409 5.34323864,-0.375028409 3.84190341,1.12508523 L1.12508523,3.84190341 C-0.375028409,5.34201705 -0.375028409,7.77420455 1.12508523,9.27553977 L13.3495455,21.5 L1.12508523,33.7244602 C-0.375028409,35.2245739 -0.375028409,37.6567614 1.12508523,39.1580966 L3.84190341,41.8749148 C5.34201705,43.3750284 7.77542614,43.3750284 9.27553977,41.8749148 L21.5,29.6504545 L33.7244602,41.8749148 C35.2245739,43.3750284 37.657983,43.3750284 39.1580966,41.8749148 L41.8749148,39.1580966 C43.3750284,37.657983 43.3750284,35.2257955 41.8749148,33.7244602 L29.6504545,21.5 Z" id="Shape" vector-effect="non-scaling-stroke"/> </g> </g> </g></g></svg>
							</span>
						</button>
						<a class="navbar-brand navbar-brand--small" href="<?php echo site_url(); ?>">
							<img alt="Logo" src="https://placehold.it/210x80"/>
						</a>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<?php get_template_part('templates/parts/header-nav'); ?>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<img class="intro_image" alt="" src="https://placehold.it/991x551">

		<div class="intro__content">
			<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>
			<p>Donec vulputate nunc accumsan, facilisis metus eget, dignissim arcu. Aliquam sodales sem vel consequat scelerisque. Nunc ornare orci sit amet mi congue, in posuere arcu maximus. Aliquam massa mi, tincidunt non ligula nec, tincidunt fermentum nisi.</p>
			<a href="#"
				title=""
				class="btn btn--secondary"
				role="button">
				Get a Quote
			</a>
			<hr>
			<div class="contact-details">
				<p>01234 567890</p>
				<p>01234 567890</p>
				<p>support@companyname.co.uk</p>
			</div>
			<form id="formCallback">
				<div class="form-group">
					<label><span class="sr-only">Phone number for callback</span>
						<input type="text" name="calbacknumber" placeholder="Enter your contact number"/>
					</label>
				</div>
				<button title="Send contact number for a callback"
				class="btn btn--secondary btn--block">Request a call back</button>
			</form>
		</div>
	</div>

</div>
