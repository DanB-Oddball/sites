<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Meet the Team</h2>
					<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-sm-8">
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<blockquote>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
					</blockquote>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-sm-4">
					<img alt="" class="img-responsive section__image" src="https://placehold.it/292"/>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section section--no-padding-top">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<!-- team 1 -->

				<div class="col-sm-12">
					<div class="card-team">
						<div class="tab-section">
							<ul class="tab-section__nav tab-nav__team nav nav-pills">
								<li class="active">
									<a href="#tab-team_a" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_b" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_c" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_d" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
							</ul>
							<div class="tab__content tab-nav__team tab-content">
								<div class="tab-pane active" id="tab-team_a">
									<div class="card-team__inner">
										<h3>Bob Hoskins</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_b">
									<div class="card-team__inner">
										<h3>Name Two</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_c">
									<div class="card-team__inner">
										<h3>Name Three</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_d">
									<div class="card-team__inner">
										<h3>Name Four</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
							</div><!-- tab content -->
						</div>

					</div>
				</div>

				<!-- team 2 -->

				<div class="col-sm-12">
					<div class="card-team">
						<div class="tab-section">
							<ul class="tab-section__nav tab-nav__team nav nav-pills">
								<li class="active">
									<a href="#tab-team_aa" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_bb" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_cc" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
								<li>
									<a href="#tab-team_dd" data-toggle="pill">
										<div class="card-team__overlay"></div>
										<img alt="" class="card-team__image" src="https://placehold.it/360x349"/>
									</a>
								</li>
							</ul>
							<div class="tab__content tab-nav__team tab-content">
								<div class="tab-pane active" id="tab-team_aa">
									<div class="card-team__inner">
										<h3>P. I. Staker</h3>
										<h4>Senior Installer</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_bb">
									<div class="card-team__inner">
										<h3>Name Two</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_cc">
									<div class="card-team__inner">
										<h3>Name Three</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
								<div class="tab-pane" id="tab-team_dd">
									<div class="card-team__inner">
										<h3>Name Four</h3>
										<h4>Company owner</h4>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
									</div>
								</div>
							</div><!-- tab content -->
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>
