<div class="banner-section"
style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banners/about-banner.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 banner__col">
				<div class="banner__content">
					<h1 class="page-title"><?php echo get_the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</div>
