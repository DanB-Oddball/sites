<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-5">
					<img alt="" class="img-responsive section__image" src="https://placehold.it/330x250"/>
				</div>
				<div class="col-sm-7">
					<h2>Lorem ipsum dolar sit amet</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ut nisi nulla. Nullam ut faucibus purus, non egestas enim. Ut laoreet et augue a bibendum. Ut ut nisl risus. Vivamus mi metus, aliquam id lacus nec, sollicitudin tempus erat. In non felis enim. Aliquam in magna nulla. Aenean eleifend ligula sit amet diam finibus egestas. Vestibulum venenatis consequat elit, sit amet ullamcorper velit auctor nec. Duis eu magna eget velit ornare semper. Nullam pulvinar est nec risus dictum, nec elementum sem rhoncus.</p>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="section section--grad" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/grad.svg);">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="h--line">Our product range</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>


				<div class="col-sm-12">
					<div class="tab-section">
						<ul class="tab-section__nav tab-nav__vertical tab-nav__product-list nav nav-pills nav-stacked col-sm-2">
							<li class="active"><a href="#tab_a" data-toggle="pill">Gas Boilers</a></li>
							<li><a href="#tab_b" data-toggle="pill">Oil Boilers</a></li>
							<li><a href="#tab_c" data-toggle="pill">Solar Panels</a></li>
							<li><a href="#tab_d" data-toggle="pill">Controls</a></li>
							<li><a href="#tab_e" data-toggle="pill">Heat Pumps</a></li>
							<li><a href="#tab_f" data-toggle="pill">Cylinders</a></li>
						</ul>
						<div class="tab__content tab__content--vertical tab__content-product-list tab-content col-sm-9">
							<div class="tab-pane active" id="tab_a">
								<div class="row">
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">7 Day Twin Channel Programmer</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">Bosch EasyControl Black</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">Greenstar Comfort Control</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">Greenstar Comfort II RF Control</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_b">
								<div class="row">
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">Bosch EasyControl Black</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">7 Day Twin Channel Programmer</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">7 Day Twin Channel Programmer</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">7 Day Twin Channel Programmer</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
									<div class="col-sm-6 col-md-4 product-card">
										<img class="product-card__image"src="https://placehold.it/140x140"/>
										<h3 class="product-card__title">7 Day Twin Channel Programmer</h3>
										<img class="product-card__brand" src="https://placehold.it/60x20" />
									</div>
								</div>
							</div>
						</div><!-- tab content -->
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
