<div class="section">
	<div class="container">
		<div class="section--inner">
			<div class="row">

				<div class="col-md-6">
					<h2 class="h--line">New boiler and solar panel fitting</h2>
					<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<blockquote>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
					</blockquote>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</div>

				<div class="col-md-push-1 col-md-5">
					<div class="image-gallery">
						<div class="image-gallery__featured">
							<img src="https://placehold.it/514x340">
						</div>
						<div class="image-gallery__tumbnails">
							<div
								class="image-gallery__thumbnail"
								data-url="use img url here so JS can updated the featured image">
								<img src="https://placehold.it/80">
							</div>
							<div
								class="image-gallery__thumbnail"
								data-url="use img url here so JS can updated the featured image">
								<img src="https://placehold.it/80">
							</div>
							<div
								class="image-gallery__thumbnail"
								data-url="use img url here so JS can updated the featured image">
								<img src="https://placehold.it/80">
							</div>
							<div
								class="image-gallery__thumbnail"
								data-url="use img url here so JS can updated the featured image">
								<img src="https://placehold.it/80">
							</div>
						</div>

						<div class="image-gallery__nav">
							<div class="image-gallery__nav-left"></div>
							<div class="image-gallery__nav-right"></div>
						</div>
					</div>
				</div>

				<div class="col-xs-12">
					<a
					href=""
					title=""
					class="btn btn--secondary btn--previous btn--small"
					role="button">
					Previous
					</a>
					<a
					href=""
					title=""
					class="btn btn--secondary btn--next btn--small"
					role="button">
					Next
					</a>
				</div>

			</div>
		</div>
	</div>
</div>
