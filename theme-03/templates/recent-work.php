<?php
/*
Template Name: Recent Work
*/
get_header();

// Banner
get_template_part('templates/parts/recent-work/banner');

// content
get_template_part('templates/parts/recent-work/content');

get_footer();
