<div class="container">
	<h4 class="underline">Typography</h4>
	<h1>H1 Heading</h1>
	<h2 class="h--line">H2 Heading</h2>
	<h3 class="h--line">H3 Heading</h3>
	<h4>H4 Heading</h4>

	<p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

	<h4 class="underline">Inline links and buttons with auto-size</h4>
	<a href="" title="" role="button" class="btn btn--primary">Read More</a>
	<a href="" title="" role="button" class="btn btn--primary">See More</a>
	<a href="" title="" role="button" class="btn btn--primary">Click Here</a>
	<a href="" title="" role="button" class="btn btn--secondary">Sign-up</a>
	<button class="btn btn--secondary">Sign-up</button>
	<button class="btn btn--primary">Submit</button>

	<h4 class="underline">Block links and buttons with size options</h4>
	<a href="" title="" role="button" class="btn btn--primary btn--block btn--small">Size Small</a>
	<a href="" title="" role="button" class="btn btn--primary btn--block btn--medium">Size Medium</a>
	<a href="" title="" role="button" class="btn btn--primary btn--block btn--large">Size Large</a>
	<a href="" title="" role="button" class="btn btn--primary btn--block">Auto-size Full Width</a>
	<a href="" title="" role="button" class="btn btn--secondary btn--block">Auto-size Full Width</a>
	<button class="btn btn--secondary btn--block">Sign-up</button>
	<button class="btn btn--primary btn--block">Submit</button>

	<h4 class="underline">Block Elements</h4>
	<blockquote>
		Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
	</blockquote>
</div>
